package hobbyroom;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import messages.Message;
import messages.InvalidMessageType;

/**
 *
 * @author anonymous
 */
public class HandleIncomingMessages implements Runnable {

    Connection connection = Connection.getInstance();
    String messageType;
    Message newMessage;
    
    
    @Override
    public void run() {
        while (true){
            try {
                newMessage = connection.recieve();
            } catch (IOException ex) {
                Logger.getLogger(HandleIncomingMessages.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(HandleIncomingMessages.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (newMessage != null){
                messageType = newMessage.getType();
                if (messageType.equals("initializeChat")){
                    connection.setChat(newMessage.getBody());
                }
                else if (messageType.equals("broadcastText")){
                    StringBuilder temp = new StringBuilder(connection.getChat());
                    temp.append(newMessage.getBody());
                    temp.append('\n');
                    connection.setChat(temp.toString());
                }
                else if (messageType.equals("ActiveUserList")){
                    connection.addActiveUsers(newMessage.getBody());
                }
            
            }
            
            
        
        }
    
    }
    
}
