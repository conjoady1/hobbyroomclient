package hobbyroom;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import messages.Message;

/**
 *
 * @author anonymous
 */
public class Connection {
    
    private static Socket socket; 
    private static InputStream instream;
    private static OutputStream outstream;
    private static ObjectInputStream reader;
    private static ObjectOutputStream writer;
    private static String username;
    private static Connection singleInstance;
    private static String Username;
    private static VBox TribeBox;
    private static TextArea chatArea;
    private static ArrayList<String> Tribes;
    private static ScrollPane scroll;
    private static VBox ActiveUsersBox;
    private static Text chatName;
    
    public Connection(){}
    
    public void Connect(){
        try {
            System.out.println("attempting to connect to server");
            socket = new Socket("127.0.0.1",8888);
            System.out.println("Successful connection");
            instream = socket.getInputStream();
            outstream = socket.getOutputStream();
            writer = new ObjectOutputStream(outstream);
            reader = new ObjectInputStream(instream);
            
            
            
        } catch (IOException ex) {
            System.out.println("error initializing streams");
        }
    }
    public static Connection getInstance(){
        if (singleInstance == null){
            singleInstance = new Connection();
            return singleInstance;
        }
        else
            return  singleInstance;
    }
    public void setUsername(String user){
        Username = user;
    }
    public String getUsername(){
        return Username;
    }
    public void send(Message message) throws IOException{
        writer.writeObject(message);
                }
    public Message recieve() throws IOException, ClassNotFoundException{
        return (Message) reader.readObject();
    }
    public void setTribeBox(VBox box){
        TribeBox = box;
    }
    public void setChatArea(TextArea chat){
        chatArea = chat;
    }
    public void setChat(String chat){
        chatArea.setText(chat);
    }
    public void setTribes(ArrayList<String> list){
        Tribes = list;
    }
    public void showTribes(){
        for (int i = 0; i < Tribes.size(); i++){
            Button newTribe = new Button();
            newTribe.setText(Tribes.get(i));
            newTribe.setOnAction(new TribeHandler(newTribe.getText()));
            //newTribe.setPrefWidth(scroll.getPrefWidth());
            TribeBox.getChildren().add(newTribe);
        }
    }
    public void setScroll(ScrollPane pane){
        scroll = pane;
    }
    public String getChat(){
        return chatArea.getText();
    }
    public void setActiveUsersBox(VBox box){
        ActiveUsersBox = box;
    }
    public void addActiveUsers(String UserList){
        Scanner scan = new Scanner(UserList);
        while (scan.hasNext()){
            Text userguy = new Text();
            userguy.setText(scan.nextLine());
            ActiveUsersBox.getChildren().add(userguy);
        }
    }
    public void setChatName(String name){
        chatName.setText(name);
    }
    public void setChatName(Text name){
        chatName = name;
    }
    
}
