/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hobbyroom;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import messages.InvalidMessageType;
import messages.Message;

/**
 *
 * @author anonymous
 */
public class loginpageController implements Initializable {
    
    private Socket socket;
     
    
    @FXML private Button startButton;
    @FXML private TextField username;
    @FXML private Text errorMessage;
    private final Connection connection = Connection.getInstance();
    
    @FXML
    public void handlestartButton(ActionEvent event){
         boolean successfulLogin;
         String UserID = username.getText();
         Message response;
         
         
         if (UserID.equals("")){
             errorMessage.setText("Must enter a username");
             errorMessage.setVisible(true);
         }
         else{
             try {
                 
                 Message setUsernameMessage = new Message("setUsername");
                 setUsernameMessage.setBody(UserID);
                 connection.send(setUsernameMessage);
                 response = connection.recieve();
                 if (response.getBody().equals("fail")){
                     errorMessage.setText("Username is already in use");
                 }
                 else if (response.getBody().equals("success")){
                    connection.setUsername(UserID);
                    Parent mainmenuparent = FXMLLoader.load(getClass().getResource("mainmenu.fxml"));
                    Scene menu = new Scene(mainmenuparent);
                    Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                    window.setScene(menu);
                    window.show();
                     
                 }
                 response = connection.recieve();
                 if (response.getType().equals("TribeList")){
                     ArrayList<String> tribelist = new ArrayList<String>();
                     Scanner scan = new Scanner(response.getBody());
                     while(scan.hasNext()){
                         tribelist.add(scan.nextLine());
                     }
                     connection.setTribes(tribelist);
                     connection.showTribes();
                 }
                 else
                     System.out.println("server did not send tribe list");
                
                Thread handleAllNewRequests = new Thread(new HandleIncomingMessages());
                handleAllNewRequests.start();
                 
                 
             } catch (InvalidMessageType ex) {
                 Logger.getLogger(loginpageController.class.getName()).log(Level.SEVERE, null, ex);
             } catch (IOException ex) {
                 Logger.getLogger(loginpageController.class.getName()).log(Level.SEVERE, null, ex);
             } catch (ClassNotFoundException ex) {
                 Logger.getLogger(loginpageController.class.getName()).log(Level.SEVERE, null, ex);
             }
             
         }
        
        
            
            
        
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
    }
}
