package hobbyroom;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import messages.InvalidMessageType;
import messages.Message;

/**
 * FXML Controller class
 *
 * @author anonymous
 */
public class MainmenuController implements Initializable {
    
    Connection connection = Connection.getInstance();
    @FXML VBox TribeBox;
    @FXML TextArea chat;
    @FXML Button SendButton;
    @FXML TextField newMessage;
    @FXML ScrollPane TribeScroll;
    @FXML VBox ActiveUsersBox;
    @FXML Text chatName;
    Scanner scan;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        connection.setTribeBox(TribeBox);
        connection.setChatArea(chat);
        connection.setScroll(TribeScroll);
        connection.setActiveUsersBox(ActiveUsersBox);
        connection.setChatName(chatName);
        
    }  
    
    public void handleSend(ActionEvent e){
        String body = newMessage.getText();
        newMessage.setText("");
        Message message = null;
        
        try {
            message = new Message("sendText");
            if (message != null){
            StringBuilder tempString = new StringBuilder(connection.getUsername());
            tempString.append(": ");
            tempString.append(body);
            message.setBody(tempString.toString());
            connection.send(message);
            }
        } catch (InvalidMessageType ex) {
            Logger.getLogger(MainmenuController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainmenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            
        
    }
    
}
