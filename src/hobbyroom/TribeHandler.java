package hobbyroom;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import messages.Message;
import messages.InvalidMessageType;

/**
 *
 * @author anonymous
 */
public class TribeHandler implements EventHandler<ActionEvent>{
    
    String TribeName;
    Connection connection = Connection.getInstance();
    
    public TribeHandler(String Tribename){
        TribeName = Tribename;
    }

    @Override
    public void handle(ActionEvent e) {
        try {
            Message message = new Message("setActiveTribe");
            message.setBody(TribeName);
            connection.send(message);
            connection.setChatName(TribeName);
            
        } catch (InvalidMessageType ex) {
            Logger.getLogger(TribeHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TribeHandler.class.getName()).log(Level.SEVERE, null, ex);
        } 
    
    }

   

    
    
}
